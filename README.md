Example for JDK BUG JDK-8059531
===============================

[JDK-8059531: Anonymous inner class unable to access in-scope variables/methods when annotated](https://bugs.openjdk.java.net/browse/JDK-8059531)

> When an anonymous inner class extending an abstract class is annotated with a TYPE_USE annotation, the implementation code is unable to access methods and variables on the local class at runtime.
> Execution results in "Exception in thread "main" java.lang.NoSuchFieldError: this$0".
> If the annotation is removed, the code will work just fine. 

This example show this bug described in the ticket.
The code is not the same as in the ticket, in order to make it a bit more simpler an clearer:


Update
------

The bug [JDK-8059531](https://bugs.openjdk.java.net/browse/JDK-8059531) was closed on 2021-04-14 with the comment
> Not reproducible using more recent JDK 8 updates. The related bug JDK-8177316 reproduces if compiled with a JDK 8 update, but *not* if compiled using --release 8 on a subsequent JDK release. The bug also does not reproduce if compiled with JDK 9 or later. 

My example did not confirm this 100%.

- bug: The issue remain in JDK8 - in the most current version: 1.8.0_302 (implementor: Eclipse Foundation) _(need to downgrade the java version in pom.xml to 8)_
- bug: The issue remain in JDK11 - in the most current versions: 11.0.13 (implementor: Eclipse Adoptium, implementor version: Temurin-11.0.13+8)) (same problem with 11.0.2 implementor: Oracle Corporation)
- fix: The the issue is gone in: JDK17: 17.0.1 - tested with implementor:
-- Oracle Corporation and 
-- Eclipse Adoptium (implementor version="Temurin-17.0.1+12")


###NOTE
One interesting point that the problem occur with 11.0.2 (implementor: Oracle Corporation) in Maven (3.6.3) and manual compiling (`javac Jdk8059531.java`) but not with Eclipse STS 4.7.
When the code is compiled with Eclipse and Java 11.0.2 then the bytecode is correct: `12: putfield  #4 // Field myVariable:Ljava/lang/String;` (see below).


Code
----

the `TYPE_USE` annotation

```
@Retention(RUNTIME)
@Target(TYPE_USE)
public @interface TypeUseAnnotation {}
```

an empty interface, needed for the anonymous class to implement
```
public interface SomeInterface {}
```

Usage of the annotation in an anonymous inner class 
```
public class Jdk8059531 {

    //fail
    public void annotated() {
        new @TypeUseAnnotation SomeInterface() {
            String myVariable = "hello world";
        };
    }

    //work
    public void noAnnotation() {
        new SomeInterface() {
            String myVariable = "hello world";
        };
    }
}
```

A invocation of method `annotated()` will fail with this exception:
```
java.lang.NoSuchFieldError: myVariable
	at de.humanfork.experiment.bug.jdk8059531.Jdk8059531$1.<init>(Jdk8059531.java:21)
	at de.humanfork.experiment.bug.jdk8059531.Jdk8059531.annotated(Jdk8059531.java:14)
```
while the not annotated class (`noAnnotation()`) works.

(The jUnit test show this behavior.)

compiler output
---------------
`javac -verbose Jdk8059531.java`

```
[parsing started SimpleFileObject[C:\Users\engelmann\git\experiment-bug-jdk8059531\src\main\java\Jdk8059531.java]]
[parsing completed 17ms]
[loading /modules/jdk.jstatd/module-info.class]
[loading /modules/java.instrument/module-info.class]
[loading /modules/jdk.scripting.nashorn.shell/module-info.class]
[loading /modules/jdk.naming.dns/module-info.class]
[loading /modules/jdk.jfr/module-info.class]
[loading /modules/jdk.zipfs/module-info.class]
[loading /modules/jdk.jconsole/module-info.class]
[loading /modules/java.security.jgss/module-info.class]
[loading /modules/java.sql/module-info.class]
[loading /modules/jdk.security.jgss/module-info.class]
[loading /modules/jdk.internal.jvmstat/module-info.class]
[loading /modules/jdk.crypto.ec/module-info.class]
[loading /modules/jdk.compiler/module-info.class]
[loading /modules/jdk.management/module-info.class]
[loading /modules/jdk.charsets/module-info.class]
[loading /modules/jdk.net/module-info.class]
[loading /modules/jdk.jdi/module-info.class]
[loading /modules/java.desktop/module-info.class]
[loading /modules/jdk.scripting.nashorn/module-info.class]
[loading /modules/jdk.jartool/module-info.class]
[loading /modules/jdk.jshell/module-info.class]
[loading /modules/jdk.unsupported/module-info.class]
[loading /modules/jdk.naming.rmi/module-info.class]
[loading /modules/java.logging/module-info.class]
[loading /modules/java.net.http/module-info.class]
[loading /modules/java.management.rmi/module-info.class]
[loading /modules/java.smartcardio/module-info.class]
[loading /modules/java.security.sasl/module-info.class]
[loading /modules/jdk.aot/module-info.class]
[loading /modules/jdk.naming.ldap/module-info.class]
[loading /modules/jdk.internal.opt/module-info.class]
[loading /modules/jdk.attach/module-info.class]
[loading /modules/jdk.internal.vm.ci/module-info.class]
[loading /modules/jdk.unsupported.desktop/module-info.class]
[loading /modules/java.rmi/module-info.class]
[loading /modules/java.xml/module-info.class]
[loading /modules/jdk.javadoc/module-info.class]
[loading /modules/java.xml.crypto/module-info.class]
[loading /modules/java.compiler/module-info.class]
[loading /modules/java.naming/module-info.class]
[loading /modules/jdk.internal.vm.compiler/module-info.class]
[loading /modules/jdk.xml.dom/module-info.class]
[loading /modules/jdk.crypto.cryptoki/module-info.class]
[loading /modules/jdk.management.jfr/module-info.class]
[loading /modules/java.prefs/module-info.class]
[loading /modules/jdk.jcmd/module-info.class]
[loading /modules/jdk.rmic/module-info.class]
[loading /modules/java.transaction.xa/module-info.class]
[loading /modules/java.se/module-info.class]
[loading /modules/jdk.localedata/module-info.class]
[loading /modules/jdk.internal.le/module-info.class]
[loading /modules/jdk.jdeps/module-info.class]
[loading /modules/jdk.editpad/module-info.class]
[loading /modules/jdk.pack/module-info.class]
[loading /modules/jdk.accessibility/module-info.class]
[loading /modules/jdk.internal.ed/module-info.class]
[loading /modules/java.datatransfer/module-info.class]
[loading /modules/jdk.sctp/module-info.class]
[loading /modules/jdk.crypto.mscapi/module-info.class]
[loading /modules/jdk.security.auth/module-info.class]
[loading /modules/java.scripting/module-info.class]
[loading /modules/jdk.jlink/module-info.class]
[loading /modules/jdk.dynalink/module-info.class]
[loading /modules/jdk.internal.vm.compiler.management/module-info.class]
[loading /modules/java.sql.rowset/module-info.class]
[loading /modules/java.base/module-info.class]
[loading /modules/jdk.jdwp.agent/module-info.class]
[loading /modules/jdk.management.agent/module-info.class]
[loading /modules/java.management/module-info.class]
[loading /modules/jdk.hotspot.agent/module-info.class]
[loading /modules/jdk.httpserver/module-info.class]
[loading /modules/jdk.jsobject/module-info.class]
[search path for source files: .]
[search path for class files: C:\Program Files\Java\jdk-11.0.8\lib\modules,.]
[loading /modules/java.base/java/lang/Object.class]
[loading /modules/java.base/java/lang/Deprecated.class]
[loading /modules/java.base/java/lang/annotation/Retention.class]
[loading /modules/java.base/java/lang/annotation/RetentionPolicy.class]
[loading /modules/java.base/java/lang/annotation/Target.class]
[loading /modules/java.base/java/lang/annotation/ElementType.class]
[checking Jdk8059531]
[loading /modules/java.base/java/io/Serializable.class]
[loading /modules/java.base/java/lang/AutoCloseable.class]
[loading .\SomeInterface.java]
[parsing started DirectoryFileObject[.:SomeInterface.java]]
[parsing completed 0ms]
[loading .\TypeUseAnnotation.java]
[parsing started DirectoryFileObject[.:TypeUseAnnotation.java]]
[parsing completed 1ms]
[loading /modules/java.base/java/lang/annotation/Annotation.class]
[loading /modules/java.base/java/lang/String.class]
[loading /modules/java.base/java/lang/Enum.class]
[loading /modules/java.base/java/lang/Comparable.class]
[wrote Jdk8059531$1.class]
[wrote Jdk8059531$2.class]
[wrote Jdk8059531.class]
[checking SomeInterface]
[wrote .\SomeInterface.class]
[checking TypeUseAnnotation]
[wrote .\TypeUseAnnotation.class]
[total 229ms]
```

javap
-----

`javap -c Jdk8059531$1.class` (the failing class with annotation)
```
Compiled from "Jdk8059531.java"
class Jdk8059531$1 implements SomeInterface {
  java.lang.String myVariable;

  final Jdk8059531 this$0;

  Jdk8059531$1(Jdk8059531);
    Code:
       0: aload_0
       1: aload_1
       2: putfield      #1                  // Field this$0:LJdk8059531;
       5: aload_0
       6: invokespecial #2                  // Method java/lang/Object."<init>":()V
       9: aload_0
      10: ldc           #3                  // String hello world
      12: putfield      #4                  // Field SomeInterface.myVariable:Ljava/lang/String;
      15: return
}
```

`javap -c Jdk8059531$2.class` (the working class without annotation) 
```
class Jdk8059531$2 implements SomeInterface {
  java.lang.String myVariable;

  final Jdk8059531 this$0;

  Jdk8059531$2(Jdk8059531);
    Code:
       0: aload_0
       1: aload_1
       2: putfield      #1                  // Field this$0:LJdk8059531;
       5: aload_0
       6: invokespecial #2                  // Method java/lang/Object."<init>":()V
       9: aload_0
      10: ldc           #3                  // String hello world
      12: putfield      #4                  // Field myVariable:Ljava/lang/String;
      15: return
}
```

The difference is in code `12`
```
12: putfield      #4                  // Field SomeInterface.myVariable:Ljava/lang/String;
```
vs.
```
12: putfield      #4                  // Field myVariable:Ljava/lang/String;
```
It seams that the annotation confuse the compiler to try to access the field in the interface `SomeInterface`.
