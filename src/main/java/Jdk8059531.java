/**
 * Anonymous inner class unable to access in-scope variables/methods when annotated.
 *
 * When an anonymous inner class extending an abstract class is annotated with a TYPE_USE annotation,
 * the implementation code is unable to access methods and variables on the local class at runtime.
 * Execution results in "Exception in thread "main" java.lang.NoSuchFieldError: java.lang.NoSuchFieldError: myVariable.
 * If the annotation is removed, the code will work just fine.
 */
public class Jdk8059531 {

    /**
     * Fails because of annotation with Target TYPE_USE
     *
     * java.lang.NoSuchFieldError: myVariable
     *   at Jdk8059531$1.<init>(Jdk8059531.java:19)
     */
    public void annotated() {
        new @TypeUseAnnotation SomeInterface() {
            String myVariable = "hello world";
        };
    }

    public void noAnnotation() {
        new SomeInterface() {
            String myVariable = "hello world";
        };
    }

}
