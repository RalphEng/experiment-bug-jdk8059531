import static java.lang.annotation.ElementType.TYPE_USE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/** This annotation is of target type {@link ElementType#TYPE_USE}. */
@Retention(RUNTIME)
@Target(TYPE_USE)
public @interface TypeUseAnnotation {

}
