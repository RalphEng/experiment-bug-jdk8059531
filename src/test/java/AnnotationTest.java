import org.junit.jupiter.api.Test;

public class AnnotationTest {

    @Test
    public void testAnnotated() throws Exception {        
        new Jdk8059531().annotated();        
    }
    
    @Test
    public void testNoAnnotation() throws Exception {        
        new Jdk8059531().noAnnotation();        
    }

}
